# ANSIBLE

## Spécificité Apache
- La version d'Apache installé devra etre la version 2.4.29
- Assurer que Apache est bien installé
- Copier le fichier apache2.conf (basqiue) et le mettre a l'emplacement /etc/apache2/apache.conf
- Si changement du fichier, redémarrer en conséquence a l'aide d'un handlers le service Apache

## Spécificité PHP
- La version de php etre la version 7 minimum
- Modifier le fichier le index.php et le placer sous /var/wwww pour qu'il soit surcharger avec une variable contenant votre prenom et également un fichier phpinfo() pour que vous puissier avoir la page php info en la contactant

## MySQL
- Créer une base de donnée, en varaibilisant le nom de cette base dans vos fichiers de varaibles
- Créer un utilisateur root avec un password (ce password sera encrypté et dans un fichier crypté)
- Créer deux utilisateurs ayant les droits sur la base de donnée avec les droits ALL et Readonly
